// karma.conf.js
module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['mocha', 'chai'],
        reporters: ['mocha'],
        browsers: ['PhantomJS'],
        browserDisconnectTolerance: 3,
        browserNoActivityTimeout: 30000,
        captureTimeout: 20000, // If browser does not capture in given timeout [ms], kill it
        singleRun: true, // Continuous Integration mode - if true, it capture browsers, run tests and exit
        files: [
            'js/tests/**/*.spec.js',
            'libs/js/tests/chai.extend.js'
        ],
        mochaReporter: {
            output: 'minimal'
        }
    });
};