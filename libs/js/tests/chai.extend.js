(function (assert) {

    var _print_undefined = function (k, v) {
        if (typeof v === 'undefined') {
            return 'undefined';
        }
        return v;
    };

    assert.cwDeepEqual = function (act, exp, message) {
        message = typeof message !== 'undefined' ? message + '\n' : '';
        assert.deepEqual(act, exp, message + JSON.stringify(act, _print_undefined) + '\ndoes not equal\n' + JSON.stringify(exp, _print_undefined) + '\n\n');
    };

    assert.elementIsPresent = function (element) {
        assert.isTrue(element.length > 0, 'Element "' + element.selector + '" is not present.');
    };

    assert.elementIsNotPresent = function (element) {
        assert.isTrue(element.length === 0, 'Element "' + element.selector + '" should not be present.');
    };

    assert.elementIsDisabled = function (element) {
        assert.isTrue(element.attr('disabled') === 'disabled', 'Element "' + element.selector + '" is not disabled.');
    };

    assert.elementIsEnabled = function (element) {
        assert.isTrue(element.attr('disabled') !== 'disabled', 'Element "' + element.selector + '" is disabled.');
    };

    assert.elementIsNotNgHidden = function (element) {
        assert.isFalse(element.hasClass('ng-hide'), 'Element has ng-hide class applied.');
    };

    assert.elementIsNgHidden = function (element) {
        assert.isTrue(element.hasClass('ng-hide'), 'Element does not have ng-hide class applied.');
    };

    assert.elementIsNotNgShown = function (element) {
        assert.isFalse(element.hasClass('ng-show'), 'Element has ng-show class applied.');
    };

    assert.elementIsNgShown = function (element) {
        assert.isTrue(element.hasClass('ng-show'), 'Element does not have ng-show class applied.');
    };

})(window.assert || {});